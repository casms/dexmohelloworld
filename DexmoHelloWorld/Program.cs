﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DexmoHelloWorld
{
    class Program
    {
        struct Finger
        {
            public bool enabled;
            public float stiffness;
            public float position;
            public bool inwardcontrol;
        }

        static void Main(string[] args)
        {
            Libdexmo.Client.Controller controller = new Libdexmo.Client.Controller();

            Random r = new Random();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Finger[] fingers = new Finger[5];

            while(true)
            {
                var f = controller.GetFrame();
                if(f != null)
                {
                    /*
                    Console.WriteLine(f.Timestamp);
                    */

                    //  controller.StopImpedanceControlFingersAll(f.Hand.DeviceAssignedId);

                    
                    controller.ImpedanceControlOneFinger(
                    f.Hand.DeviceAssignedId,
                    Libdexmo.Model.FingerType.Index,
                    1f,
                    0.157f + ((float)r.NextDouble() / 1000f),
                    true);
                    

                    /*
                    fingers[1].stiffness = 1.0f;
                    fingers[1].position = 0.157f + ((float)r.NextDouble() / 1000f);
                    fingers[1].enabled = true;
                    fingers[1].inwardcontrol = true;

                    controller.ImpedanceControlFingers(f.Hand.DeviceAssignedId,
                        fingers.Select(x => x.enabled).ToArray(),
                        fingers.Select(x => x.stiffness).ToArray(),
                        fingers.Select(x => x.position).ToArray(),
                        fingers.Select(x => x.inwardcontrol).ToArray()
                        );
                    */

                    var hand = controller.LatestHands.First().Value;

                    //System.Console.Write(hand.IndexFinger.Joints.First().RotationNormalized[2]);
                    /*
                    foreach (var value in hand.IndexFinger.Joints) {
                        System.Console.Write(value.RotationNormalized[0] + " ");
                        System.Console.Write(value.RotationNormalized[1] + " ");
                        System.Console.Write(value.RotationNormalized[2] + " ");
                    }
                    */
                    //System.Console.WriteLine();

                    if (stopwatch.ElapsedMilliseconds > 0) {
                        System.Console.Write(string.Format("{0:D7} fps\r", (int)(1000f / (float)stopwatch.ElapsedMilliseconds)));
                    }

                    stopwatch.Reset();
                    stopwatch.Start();

                    System.Threading.Thread.Sleep(0);
                }
            }
        }
    }
}
